<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_c6f06d42be8bf90389e58359ef7b6d90c87b52f4126969fecd20e7ec7bc4287d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f5cf0665a3b6861fc74fcea48b9fd616b671e103f10264debc05190a412f7b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f5cf0665a3b6861fc74fcea48b9fd616b671e103f10264debc05190a412f7b2->enter($__internal_2f5cf0665a3b6861fc74fcea48b9fd616b671e103f10264debc05190a412f7b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_3ec0e4466f38aa6aa8dbe27b040a532b3894f2044452a5279f4f213f6f0f3ab2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ec0e4466f38aa6aa8dbe27b040a532b3894f2044452a5279f4f213f6f0f3ab2->enter($__internal_3ec0e4466f38aa6aa8dbe27b040a532b3894f2044452a5279f4f213f6f0f3ab2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_2f5cf0665a3b6861fc74fcea48b9fd616b671e103f10264debc05190a412f7b2->leave($__internal_2f5cf0665a3b6861fc74fcea48b9fd616b671e103f10264debc05190a412f7b2_prof);

        
        $__internal_3ec0e4466f38aa6aa8dbe27b040a532b3894f2044452a5279f4f213f6f0f3ab2->leave($__internal_3ec0e4466f38aa6aa8dbe27b040a532b3894f2044452a5279f4f213f6f0f3ab2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
