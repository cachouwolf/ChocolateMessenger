<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_72111f2bb34a4692c3c0a7e55cc41e467ad2b8c3530294bfd71f5611508f0b06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_010e11515525096618beb928919425715f53df3eec145d7cd387509ef98dded2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_010e11515525096618beb928919425715f53df3eec145d7cd387509ef98dded2->enter($__internal_010e11515525096618beb928919425715f53df3eec145d7cd387509ef98dded2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_ee00f89bb253c3ec9b4da2cc1361166a700717031a2f192b6b3fd094e9c55e8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee00f89bb253c3ec9b4da2cc1361166a700717031a2f192b6b3fd094e9c55e8e->enter($__internal_ee00f89bb253c3ec9b4da2cc1361166a700717031a2f192b6b3fd094e9c55e8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_010e11515525096618beb928919425715f53df3eec145d7cd387509ef98dded2->leave($__internal_010e11515525096618beb928919425715f53df3eec145d7cd387509ef98dded2_prof);

        
        $__internal_ee00f89bb253c3ec9b4da2cc1361166a700717031a2f192b6b3fd094e9c55e8e->leave($__internal_ee00f89bb253c3ec9b4da2cc1361166a700717031a2f192b6b3fd094e9c55e8e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
