<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_d7dbf3db3bb7bed2a3f4feef7501c2394c961991fa14539e0d0c8dba0b6de77c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fea1d1428297f9b4063229ea14371bd035f0ccfc1496bdfe249601ef13a3d32d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fea1d1428297f9b4063229ea14371bd035f0ccfc1496bdfe249601ef13a3d32d->enter($__internal_fea1d1428297f9b4063229ea14371bd035f0ccfc1496bdfe249601ef13a3d32d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_c372c83ee89cfb084f386f34caa787881b77c98d3f58c8d32d34fa865d7f084d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c372c83ee89cfb084f386f34caa787881b77c98d3f58c8d32d34fa865d7f084d->enter($__internal_c372c83ee89cfb084f386f34caa787881b77c98d3f58c8d32d34fa865d7f084d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_fea1d1428297f9b4063229ea14371bd035f0ccfc1496bdfe249601ef13a3d32d->leave($__internal_fea1d1428297f9b4063229ea14371bd035f0ccfc1496bdfe249601ef13a3d32d_prof);

        
        $__internal_c372c83ee89cfb084f386f34caa787881b77c98d3f58c8d32d34fa865d7f084d->leave($__internal_c372c83ee89cfb084f386f34caa787881b77c98d3f58c8d32d34fa865d7f084d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
