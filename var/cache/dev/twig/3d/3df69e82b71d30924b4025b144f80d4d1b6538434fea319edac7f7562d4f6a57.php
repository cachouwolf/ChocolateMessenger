<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_4637bdf881b03616502cff663e952e1de11fb13ee777e5412be7d6c409e8894f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d3bf6a1532bbc4348c71449bc60b80796f5e61f804482496b5f1f98e4d2182f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d3bf6a1532bbc4348c71449bc60b80796f5e61f804482496b5f1f98e4d2182f->enter($__internal_4d3bf6a1532bbc4348c71449bc60b80796f5e61f804482496b5f1f98e4d2182f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_da110583a445abbc3793d605ecce5a82fe06666e9a96de2e026640eb31807012 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da110583a445abbc3793d605ecce5a82fe06666e9a96de2e026640eb31807012->enter($__internal_da110583a445abbc3793d605ecce5a82fe06666e9a96de2e026640eb31807012_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_4d3bf6a1532bbc4348c71449bc60b80796f5e61f804482496b5f1f98e4d2182f->leave($__internal_4d3bf6a1532bbc4348c71449bc60b80796f5e61f804482496b5f1f98e4d2182f_prof);

        
        $__internal_da110583a445abbc3793d605ecce5a82fe06666e9a96de2e026640eb31807012->leave($__internal_da110583a445abbc3793d605ecce5a82fe06666e9a96de2e026640eb31807012_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
