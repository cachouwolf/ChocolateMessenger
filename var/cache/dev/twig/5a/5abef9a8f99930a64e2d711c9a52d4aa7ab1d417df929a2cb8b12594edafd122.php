<?php

/* @WebProfiler/Icon/forward.svg */
class __TwigTemplate_4ebf7a7f7c091ea0774c547c7e1ef43901513d008b018c3438aa52a357906c0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_803f01306a335d8d7504eeb5e9e44b3a0f1f3f3d06091ddea9a99a3a4a6d98be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_803f01306a335d8d7504eeb5e9e44b3a0f1f3f3d06091ddea9a99a3a4a6d98be->enter($__internal_803f01306a335d8d7504eeb5e9e44b3a0f1f3f3d06091ddea9a99a3a4a6d98be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        $__internal_23a74a8bc3aea1edb30c6e88a212e66d83401548219538279716299364a2d16a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23a74a8bc3aea1edb30c6e88a212e66d83401548219538279716299364a2d16a->enter($__internal_23a74a8bc3aea1edb30c6e88a212e66d83401548219538279716299364a2d16a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $__internal_803f01306a335d8d7504eeb5e9e44b3a0f1f3f3d06091ddea9a99a3a4a6d98be->leave($__internal_803f01306a335d8d7504eeb5e9e44b3a0f1f3f3d06091ddea9a99a3a4a6d98be_prof);

        
        $__internal_23a74a8bc3aea1edb30c6e88a212e66d83401548219538279716299364a2d16a->leave($__internal_23a74a8bc3aea1edb30c6e88a212e66d83401548219538279716299364a2d16a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
", "@WebProfiler/Icon/forward.svg", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/forward.svg");
    }
}
