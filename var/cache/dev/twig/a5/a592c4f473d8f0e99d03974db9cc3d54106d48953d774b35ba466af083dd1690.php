<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_0bf77a47730e02fcf23c94ec7e89c784ea00fe6ec14f2c3a6da988c62afea9a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_303f033c263b9292d7c666b5006dfc2dacb5dd838228bfbb56d884858e03092b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_303f033c263b9292d7c666b5006dfc2dacb5dd838228bfbb56d884858e03092b->enter($__internal_303f033c263b9292d7c666b5006dfc2dacb5dd838228bfbb56d884858e03092b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_98ca4ef0e2e860d9cd9c04433f2433d131dc396e30d7421d12fe4f4bbdb03b26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98ca4ef0e2e860d9cd9c04433f2433d131dc396e30d7421d12fe4f4bbdb03b26->enter($__internal_98ca4ef0e2e860d9cd9c04433f2433d131dc396e30d7421d12fe4f4bbdb03b26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_303f033c263b9292d7c666b5006dfc2dacb5dd838228bfbb56d884858e03092b->leave($__internal_303f033c263b9292d7c666b5006dfc2dacb5dd838228bfbb56d884858e03092b_prof);

        
        $__internal_98ca4ef0e2e860d9cd9c04433f2433d131dc396e30d7421d12fe4f4bbdb03b26->leave($__internal_98ca4ef0e2e860d9cd9c04433f2433d131dc396e30d7421d12fe4f4bbdb03b26_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
