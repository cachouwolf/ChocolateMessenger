<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_2ace783b508e2369b5be6fdb8cc26a46e20f9bb55f0df8c9256f2d9027f272e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9be497a238b3bb1479263ea96ee41693885dab1ee0172131f0e694fbf8ee63d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9be497a238b3bb1479263ea96ee41693885dab1ee0172131f0e694fbf8ee63d6->enter($__internal_9be497a238b3bb1479263ea96ee41693885dab1ee0172131f0e694fbf8ee63d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_b4d175b89efbf24707e4576c2e4a1020fc68264ff9f7c549f3b86dd99b0f9065 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4d175b89efbf24707e4576c2e4a1020fc68264ff9f7c549f3b86dd99b0f9065->enter($__internal_b4d175b89efbf24707e4576c2e4a1020fc68264ff9f7c549f3b86dd99b0f9065_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_9be497a238b3bb1479263ea96ee41693885dab1ee0172131f0e694fbf8ee63d6->leave($__internal_9be497a238b3bb1479263ea96ee41693885dab1ee0172131f0e694fbf8ee63d6_prof);

        
        $__internal_b4d175b89efbf24707e4576c2e4a1020fc68264ff9f7c549f3b86dd99b0f9065->leave($__internal_b4d175b89efbf24707e4576c2e4a1020fc68264ff9f7c549f3b86dd99b0f9065_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
