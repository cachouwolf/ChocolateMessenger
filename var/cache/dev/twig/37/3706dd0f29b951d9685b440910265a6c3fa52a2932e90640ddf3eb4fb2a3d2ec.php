<?php

/* :Author:edit.html.twig */
class __TwigTemplate_5e2f7b71edbf4ec929f0eb1ff0ec99d7b89273e4e02b28e6aad6dc5a675198b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Author:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e49dfc21dd103f611d7be8a1af36d989c3117fabc98e3359477095f8ceb0c58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e49dfc21dd103f611d7be8a1af36d989c3117fabc98e3359477095f8ceb0c58->enter($__internal_3e49dfc21dd103f611d7be8a1af36d989c3117fabc98e3359477095f8ceb0c58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:edit.html.twig"));

        $__internal_d3c8965b8a7c600a11cfd97642f89c99f625cc0b436a2b2d4c2c136581ad06e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3c8965b8a7c600a11cfd97642f89c99f625cc0b436a2b2d4c2c136581ad06e3->enter($__internal_d3c8965b8a7c600a11cfd97642f89c99f625cc0b436a2b2d4c2c136581ad06e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:edit.html.twig"));

        // line 2
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme(($context["edit_form"] ?? $this->getContext($context, "edit_form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3e49dfc21dd103f611d7be8a1af36d989c3117fabc98e3359477095f8ceb0c58->leave($__internal_3e49dfc21dd103f611d7be8a1af36d989c3117fabc98e3359477095f8ceb0c58_prof);

        
        $__internal_d3c8965b8a7c600a11cfd97642f89c99f625cc0b436a2b2d4c2c136581ad06e3->leave($__internal_d3c8965b8a7c600a11cfd97642f89c99f625cc0b436a2b2d4c2c136581ad06e3_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_bb527d9bc06e3332f932f40aa9793adf9e9c361784b63ba123f574645ea4ca32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb527d9bc06e3332f932f40aa9793adf9e9c361784b63ba123f574645ea4ca32->enter($__internal_bb527d9bc06e3332f932f40aa9793adf9e9c361784b63ba123f574645ea4ca32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a9367f64c2a751e6bb19ea8d6feae83f5e0fa787b7b859dee2c355711b8db59d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9367f64c2a751e6bb19ea8d6feae83f5e0fa787b7b859dee2c355711b8db59d->enter($__internal_a9367f64c2a751e6bb19ea8d6feae83f5e0fa787b7b859dee2c355711b8db59d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <div class=\"container\">
      <div class=\"col-xs-12\">
          <h1 class=\"title\">Edit author </h1>
          <div class=\"row btnedit\">
            ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
            <div class=\"col-md-10\">

              ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
            </div>
            <div class=\"col-md-2\"/>
            </div>
            <div class=\"col-md-10\">
              <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Modifier\" />
            </div>
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "
            <div class=\"col-md-2 icon\"/>
              <a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_index");
        echo "\"><img class=\"icon-return\"src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/return.png"), "html", null, true);
        echo "\" alt=\"Return\"/></a>
              ";
        // line 22
        if ($this->getAttribute($this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "messages", array()), "isEmpty", array(), "method")) {
            // line 23
            echo "                ";
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
            echo "
                  <input class=\"icon-delete\" type=\"image\" src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/remove.png"), "html", null, true);
            echo "\" alt=\"delete\"/>
                ";
            // line 25
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
            echo "
              ";
        }
        // line 27
        echo "            </div>
          </div>


      </div>
    </div>








";
        
        $__internal_a9367f64c2a751e6bb19ea8d6feae83f5e0fa787b7b859dee2c355711b8db59d->leave($__internal_a9367f64c2a751e6bb19ea8d6feae83f5e0fa787b7b859dee2c355711b8db59d_prof);

        
        $__internal_bb527d9bc06e3332f932f40aa9793adf9e9c361784b63ba123f574645ea4ca32->leave($__internal_bb527d9bc06e3332f932f40aa9793adf9e9c361784b63ba123f574645ea4ca32_prof);

    }

    public function getTemplateName()
    {
        return ":Author:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 27,  96 => 25,  92 => 24,  87 => 23,  85 => 22,  79 => 21,  74 => 19,  64 => 12,  58 => 9,  52 => 5,  43 => 4,  33 => 1,  31 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% form_theme edit_form 'bootstrap_3_layout.html.twig' %}

{% block body %}
    <div class=\"container\">
      <div class=\"col-xs-12\">
          <h1 class=\"title\">Edit author </h1>
          <div class=\"row btnedit\">
            {{ form_start(edit_form) }}
            <div class=\"col-md-10\">

              {{ form_widget(edit_form) }}
            </div>
            <div class=\"col-md-2\"/>
            </div>
            <div class=\"col-md-10\">
              <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Modifier\" />
            </div>
            {{ form_end(edit_form) }}
            <div class=\"col-md-2 icon\"/>
              <a href=\"{{ path('author_index') }}\"><img class=\"icon-return\"src=\"{{ asset('img/return.png') }}\" alt=\"Return\"/></a>
              {% if author.messages.isEmpty() %}
                {{ form_start(delete_form) }}
                  <input class=\"icon-delete\" type=\"image\" src=\"{{ asset('img/remove.png') }}\" alt=\"delete\"/>
                {{ form_end(delete_form) }}
              {% endif %}
            </div>
          </div>


      </div>
    </div>








{% endblock %}
", ":Author:edit.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Author/edit.html.twig");
    }
}
