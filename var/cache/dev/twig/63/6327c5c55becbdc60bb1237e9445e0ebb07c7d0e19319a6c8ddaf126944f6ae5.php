<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_3778406ef85a6637d2a2a0be9d378272396e22464ec59695638d364213c2a4f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a24eecf79e3af4ed0db97421008b7dafec152d7c113381e1fdce682fa692f02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a24eecf79e3af4ed0db97421008b7dafec152d7c113381e1fdce682fa692f02->enter($__internal_5a24eecf79e3af4ed0db97421008b7dafec152d7c113381e1fdce682fa692f02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_d74091ad4a820ef66b4ff5dd4f5d3f05da1210608fee421c63e145333bdb48ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d74091ad4a820ef66b4ff5dd4f5d3f05da1210608fee421c63e145333bdb48ca->enter($__internal_d74091ad4a820ef66b4ff5dd4f5d3f05da1210608fee421c63e145333bdb48ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_5a24eecf79e3af4ed0db97421008b7dafec152d7c113381e1fdce682fa692f02->leave($__internal_5a24eecf79e3af4ed0db97421008b7dafec152d7c113381e1fdce682fa692f02_prof);

        
        $__internal_d74091ad4a820ef66b4ff5dd4f5d3f05da1210608fee421c63e145333bdb48ca->leave($__internal_d74091ad4a820ef66b4ff5dd4f5d3f05da1210608fee421c63e145333bdb48ca_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_af88a7d8d9a4b3eadc5281de5a4a6be76aa720513d79746bc8c7fd5ccd94e6df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af88a7d8d9a4b3eadc5281de5a4a6be76aa720513d79746bc8c7fd5ccd94e6df->enter($__internal_af88a7d8d9a4b3eadc5281de5a4a6be76aa720513d79746bc8c7fd5ccd94e6df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_bc6fa60dc25095e013cc94610f07835e9bcda6ea6195ace894b9435475942e11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc6fa60dc25095e013cc94610f07835e9bcda6ea6195ace894b9435475942e11->enter($__internal_bc6fa60dc25095e013cc94610f07835e9bcda6ea6195ace894b9435475942e11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_bc6fa60dc25095e013cc94610f07835e9bcda6ea6195ace894b9435475942e11->leave($__internal_bc6fa60dc25095e013cc94610f07835e9bcda6ea6195ace894b9435475942e11_prof);

        
        $__internal_af88a7d8d9a4b3eadc5281de5a4a6be76aa720513d79746bc8c7fd5ccd94e6df->leave($__internal_af88a7d8d9a4b3eadc5281de5a4a6be76aa720513d79746bc8c7fd5ccd94e6df_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
