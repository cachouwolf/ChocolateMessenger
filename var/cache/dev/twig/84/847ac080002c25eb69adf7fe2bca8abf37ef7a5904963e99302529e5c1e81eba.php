<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_43cfa4df6fa507a07dd290b4b81fca69b70f03cab0b64e18100a287cfa1b70c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d2df468de1d318ddb45b9a58aa82013d6b35f7eb642e9d9ff90993ee397cde5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d2df468de1d318ddb45b9a58aa82013d6b35f7eb642e9d9ff90993ee397cde5->enter($__internal_7d2df468de1d318ddb45b9a58aa82013d6b35f7eb642e9d9ff90993ee397cde5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_e1a36b2bad1f36a87239d3de0caf92cdbb20020ee33a5d72a6ce7c5a8b036dbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1a36b2bad1f36a87239d3de0caf92cdbb20020ee33a5d72a6ce7c5a8b036dbc->enter($__internal_e1a36b2bad1f36a87239d3de0caf92cdbb20020ee33a5d72a6ce7c5a8b036dbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_7d2df468de1d318ddb45b9a58aa82013d6b35f7eb642e9d9ff90993ee397cde5->leave($__internal_7d2df468de1d318ddb45b9a58aa82013d6b35f7eb642e9d9ff90993ee397cde5_prof);

        
        $__internal_e1a36b2bad1f36a87239d3de0caf92cdbb20020ee33a5d72a6ce7c5a8b036dbc->leave($__internal_e1a36b2bad1f36a87239d3de0caf92cdbb20020ee33a5d72a6ce7c5a8b036dbc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
