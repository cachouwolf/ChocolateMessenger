<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_67b6c4b75c73ea256d9274ba7abce871d65d9a6f616988fe80da1cc8701da5cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed347944e4f4e9c176a938a5c80653897716c25502491cf5f96977fd8a542b2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed347944e4f4e9c176a938a5c80653897716c25502491cf5f96977fd8a542b2e->enter($__internal_ed347944e4f4e9c176a938a5c80653897716c25502491cf5f96977fd8a542b2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_3652d8120bd6848fab8ac5630eb60e6125b4f8ac9778510c583a79ff46c6882d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3652d8120bd6848fab8ac5630eb60e6125b4f8ac9778510c583a79ff46c6882d->enter($__internal_3652d8120bd6848fab8ac5630eb60e6125b4f8ac9778510c583a79ff46c6882d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_ed347944e4f4e9c176a938a5c80653897716c25502491cf5f96977fd8a542b2e->leave($__internal_ed347944e4f4e9c176a938a5c80653897716c25502491cf5f96977fd8a542b2e_prof);

        
        $__internal_3652d8120bd6848fab8ac5630eb60e6125b4f8ac9778510c583a79ff46c6882d->leave($__internal_3652d8120bd6848fab8ac5630eb60e6125b4f8ac9778510c583a79ff46c6882d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
