<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_14006793ff0e0fc11971a0eb9ccd995c1b59242fb07f6c2c641d8b3cbcf95cac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_282902dc042f54d9bf58b53f497dd7c1c1fcb8bb9d9fa8f65d775e4427c2f764 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_282902dc042f54d9bf58b53f497dd7c1c1fcb8bb9d9fa8f65d775e4427c2f764->enter($__internal_282902dc042f54d9bf58b53f497dd7c1c1fcb8bb9d9fa8f65d775e4427c2f764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_0653809847235fea552d87ecedb2619dbe31fbde574921d60f7f414b45400e2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0653809847235fea552d87ecedb2619dbe31fbde574921d60f7f414b45400e2a->enter($__internal_0653809847235fea552d87ecedb2619dbe31fbde574921d60f7f414b45400e2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_282902dc042f54d9bf58b53f497dd7c1c1fcb8bb9d9fa8f65d775e4427c2f764->leave($__internal_282902dc042f54d9bf58b53f497dd7c1c1fcb8bb9d9fa8f65d775e4427c2f764_prof);

        
        $__internal_0653809847235fea552d87ecedb2619dbe31fbde574921d60f7f414b45400e2a->leave($__internal_0653809847235fea552d87ecedb2619dbe31fbde574921d60f7f414b45400e2a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
