<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_d9ffae65641b2646830901a5f86c06bf780d0129aa15524b0693f8abf9d9bfc6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6c1ef369c29756d509491ad89d3226005d3159d75c36277bd90bba7d7742804b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c1ef369c29756d509491ad89d3226005d3159d75c36277bd90bba7d7742804b->enter($__internal_6c1ef369c29756d509491ad89d3226005d3159d75c36277bd90bba7d7742804b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_0b1bf082d4ee2bcaed6cfffcaafc4da91f1d589de6a534f7dcaab1ebdcf791de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b1bf082d4ee2bcaed6cfffcaafc4da91f1d589de6a534f7dcaab1ebdcf791de->enter($__internal_0b1bf082d4ee2bcaed6cfffcaafc4da91f1d589de6a534f7dcaab1ebdcf791de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6c1ef369c29756d509491ad89d3226005d3159d75c36277bd90bba7d7742804b->leave($__internal_6c1ef369c29756d509491ad89d3226005d3159d75c36277bd90bba7d7742804b_prof);

        
        $__internal_0b1bf082d4ee2bcaed6cfffcaafc4da91f1d589de6a534f7dcaab1ebdcf791de->leave($__internal_0b1bf082d4ee2bcaed6cfffcaafc4da91f1d589de6a534f7dcaab1ebdcf791de_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b66c3aa1217e9e50a63f23b1281b70eb9380e120d1b886af431eecc76dd037cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b66c3aa1217e9e50a63f23b1281b70eb9380e120d1b886af431eecc76dd037cf->enter($__internal_b66c3aa1217e9e50a63f23b1281b70eb9380e120d1b886af431eecc76dd037cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c55806b77e5fa23068308ae8b2c9f5ba76ee94d2e5a73608935696b8e5bfbc94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c55806b77e5fa23068308ae8b2c9f5ba76ee94d2e5a73608935696b8e5bfbc94->enter($__internal_c55806b77e5fa23068308ae8b2c9f5ba76ee94d2e5a73608935696b8e5bfbc94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_c55806b77e5fa23068308ae8b2c9f5ba76ee94d2e5a73608935696b8e5bfbc94->leave($__internal_c55806b77e5fa23068308ae8b2c9f5ba76ee94d2e5a73608935696b8e5bfbc94_prof);

        
        $__internal_b66c3aa1217e9e50a63f23b1281b70eb9380e120d1b886af431eecc76dd037cf->leave($__internal_b66c3aa1217e9e50a63f23b1281b70eb9380e120d1b886af431eecc76dd037cf_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c5c15da4cb7bbb5827fb81e03602449a664d3ce92d12f8697c7f82809701d6fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5c15da4cb7bbb5827fb81e03602449a664d3ce92d12f8697c7f82809701d6fc->enter($__internal_c5c15da4cb7bbb5827fb81e03602449a664d3ce92d12f8697c7f82809701d6fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_784298438de0e849acbaaee1f6dfd91b47578744a9e6b51a30ae9485efed29a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_784298438de0e849acbaaee1f6dfd91b47578744a9e6b51a30ae9485efed29a5->enter($__internal_784298438de0e849acbaaee1f6dfd91b47578744a9e6b51a30ae9485efed29a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_784298438de0e849acbaaee1f6dfd91b47578744a9e6b51a30ae9485efed29a5->leave($__internal_784298438de0e849acbaaee1f6dfd91b47578744a9e6b51a30ae9485efed29a5_prof);

        
        $__internal_c5c15da4cb7bbb5827fb81e03602449a664d3ce92d12f8697c7f82809701d6fc->leave($__internal_c5c15da4cb7bbb5827fb81e03602449a664d3ce92d12f8697c7f82809701d6fc_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
