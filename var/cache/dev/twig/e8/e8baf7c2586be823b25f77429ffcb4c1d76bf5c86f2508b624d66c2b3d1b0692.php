<?php

/* :Message:index.html.twig */
class __TwigTemplate_b4e06fad6d77180acb668f4eab56b0face83b135fe9dbd7b1840c784767b159c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Message:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_08f24ecaa379e04f2ff870d5a14ed0962524e64dbe2652e203d9f25f8f01b9ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08f24ecaa379e04f2ff870d5a14ed0962524e64dbe2652e203d9f25f8f01b9ad->enter($__internal_08f24ecaa379e04f2ff870d5a14ed0962524e64dbe2652e203d9f25f8f01b9ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:index.html.twig"));

        $__internal_12e44587c9f68fb4271604d745e281353b984f37e01deb3de07c69a87c23a0ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12e44587c9f68fb4271604d745e281353b984f37e01deb3de07c69a87c23a0ac->enter($__internal_12e44587c9f68fb4271604d745e281353b984f37e01deb3de07c69a87c23a0ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_08f24ecaa379e04f2ff870d5a14ed0962524e64dbe2652e203d9f25f8f01b9ad->leave($__internal_08f24ecaa379e04f2ff870d5a14ed0962524e64dbe2652e203d9f25f8f01b9ad_prof);

        
        $__internal_12e44587c9f68fb4271604d745e281353b984f37e01deb3de07c69a87c23a0ac->leave($__internal_12e44587c9f68fb4271604d745e281353b984f37e01deb3de07c69a87c23a0ac_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e70a335775f9a5e4e8fd47ef3f3efd544dd4d14bb69fbc6690cb3c13d725ed46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e70a335775f9a5e4e8fd47ef3f3efd544dd4d14bb69fbc6690cb3c13d725ed46->enter($__internal_e70a335775f9a5e4e8fd47ef3f3efd544dd4d14bb69fbc6690cb3c13d725ed46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_de68bd94e5c1761ba0859f6f72ed7cbd0eb47563cf59e2f6a23acab58efc516b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de68bd94e5c1761ba0859f6f72ed7cbd0eb47563cf59e2f6a23acab58efc516b->enter($__internal_de68bd94e5c1761ba0859f6f72ed7cbd0eb47563cf59e2f6a23acab58efc516b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "

    <!-- Button add task-->
    <div class=\"container\">
      <div class=\"row add\">
        <h1 class=\"title\">Chocolate Messenger </h1>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_new");
        echo "\">
          <div class=\"col-xs-12 \">
            <button type=\"button\" class=\"btn btn-default btnadd\">
              <img class=\"addimg\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/add.png"), "html", null, true);
        echo "\" alt=\"add\"/>
              <p class=\"addp\">Add a message</p>
            </button>
          </div>
        </a>
      </div>
    </div>

    <!-- Task -->
    <div class=\"container\">
      <div class=\"row message\">
        ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["messages"] ?? $this->getContext($context, "messages")));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 25
            echo "        <div class=\"col-xs-9\">
          <p class=\"object\">";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["message"], "object", array()), "html", null, true);
            echo "</p>
          <p class=\"message\">";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["message"], "message", array()), "html", null, true);
            echo "</p>
        </div>
        <div class=\"col-xs-2\">
          <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_edit", array("id" => $this->getAttribute($context["message"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/modify.png"), "html", null, true);
            echo "\" alt=\"modifier\"/></a>
          <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_show", array("id" => $this->getAttribute($context["message"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/view.png"), "html", null, true);
            echo "\" alt=\"En savoir plus\"/></a>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "      </div>
    </div>

";
        
        $__internal_de68bd94e5c1761ba0859f6f72ed7cbd0eb47563cf59e2f6a23acab58efc516b->leave($__internal_de68bd94e5c1761ba0859f6f72ed7cbd0eb47563cf59e2f6a23acab58efc516b_prof);

        
        $__internal_e70a335775f9a5e4e8fd47ef3f3efd544dd4d14bb69fbc6690cb3c13d725ed46->leave($__internal_e70a335775f9a5e4e8fd47ef3f3efd544dd4d14bb69fbc6690cb3c13d725ed46_prof);

    }

    public function getTemplateName()
    {
        return ":Message:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 34,  100 => 31,  94 => 30,  88 => 27,  84 => 26,  81 => 25,  77 => 24,  63 => 13,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}


    <!-- Button add task-->
    <div class=\"container\">
      <div class=\"row add\">
        <h1 class=\"title\">Chocolate Messenger </h1>
        <a href=\"{{ path('message_new') }}\">
          <div class=\"col-xs-12 \">
            <button type=\"button\" class=\"btn btn-default btnadd\">
              <img class=\"addimg\" src=\"{{ asset('img/add.png') }}\" alt=\"add\"/>
              <p class=\"addp\">Add a message</p>
            </button>
          </div>
        </a>
      </div>
    </div>

    <!-- Task -->
    <div class=\"container\">
      <div class=\"row message\">
        {% for message in messages %}
        <div class=\"col-xs-9\">
          <p class=\"object\">{{ message.object }}</p>
          <p class=\"message\">{{ message.message }}</p>
        </div>
        <div class=\"col-xs-2\">
          <a href=\"{{ path('message_edit', { 'id': message.id }) }}\"><img src=\"{{ asset('img/modify.png') }}\" alt=\"modifier\"/></a>
          <a href=\"{{ path('message_show', { 'id': message.id }) }}\"><img src=\"{{ asset('img/view.png') }}\" alt=\"En savoir plus\"/></a>
        </div>
        {% endfor %}
      </div>
    </div>

{% endblock %}
", ":Message:index.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Message/index.html.twig");
    }
}
