<?php

/* :Author:index.html.twig */
class __TwigTemplate_7ff9d6fba9fa0aad87990531cdbec755d1ac63e9f94b8cdc222c4f7fb227b213 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Author:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b565bc8f8428c29e4273e58c358aaf7703cd6ba50cd5c35024c2c12132d23889 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b565bc8f8428c29e4273e58c358aaf7703cd6ba50cd5c35024c2c12132d23889->enter($__internal_b565bc8f8428c29e4273e58c358aaf7703cd6ba50cd5c35024c2c12132d23889_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:index.html.twig"));

        $__internal_0f74aea993e0157bbb151bd729168b28cbd03b9f7028744ea57859086354586d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f74aea993e0157bbb151bd729168b28cbd03b9f7028744ea57859086354586d->enter($__internal_0f74aea993e0157bbb151bd729168b28cbd03b9f7028744ea57859086354586d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b565bc8f8428c29e4273e58c358aaf7703cd6ba50cd5c35024c2c12132d23889->leave($__internal_b565bc8f8428c29e4273e58c358aaf7703cd6ba50cd5c35024c2c12132d23889_prof);

        
        $__internal_0f74aea993e0157bbb151bd729168b28cbd03b9f7028744ea57859086354586d->leave($__internal_0f74aea993e0157bbb151bd729168b28cbd03b9f7028744ea57859086354586d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_a9dd1d7dadbf2aaec8c9aba5cba57f243b5896e69666affe9370f886bebd014d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9dd1d7dadbf2aaec8c9aba5cba57f243b5896e69666affe9370f886bebd014d->enter($__internal_a9dd1d7dadbf2aaec8c9aba5cba57f243b5896e69666affe9370f886bebd014d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3acb802d6453bcff7de005db56db6e02d3e892bec95b6ff6f5569230c65b784c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3acb802d6453bcff7de005db56db6e02d3e892bec95b6ff6f5569230c65b784c->enter($__internal_3acb802d6453bcff7de005db56db6e02d3e892bec95b6ff6f5569230c65b784c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "




    <!-- Button add task-->
    <div class=\"container\">
      <div class=\"row add\">
      <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_new");
        echo "\">
          <div class=\"col-xs-12 \">
            <button type=\"button\" class=\"btn btn-default btnadd\">
              <img class=\"addimg\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/add.png"), "html", null, true);
        echo "\" alt=\"add\"/>
              <p class=\"addp\">Add a author</p>
            </button>
          </div>
        </a>
      </div>
    </div>

    <!-- Task -->
    <div class=\"container\">
      <div class=\"row message\">
        ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["authors"] ?? $this->getContext($context, "authors")));
        foreach ($context['_seq'] as $context["_key"] => $context["author"]) {
            // line 27
            echo "
        <div class=\"col-xs-9\">
          <p class=\"firstName\">";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["author"], "FirstName", array()), "html", null, true);
            echo "</p>
          <p class=\"lastName\">";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["author"], "LastName", array()), "html", null, true);
            echo "</p>
          <p class=\"gender\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["author"], "gender", array()), "html", null, true);
            echo "</p>



        </div>
        <div class=\"col-xs-2\">
          <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_edit", array("id" => $this->getAttribute($context["author"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/modify.png"), "html", null, true);
            echo "\" alt=\"edit\"/></a>
          <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_show", array("id" => $this->getAttribute($context["author"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/view.png"), "html", null, true);
            echo "\" alt=\"More\"/></a>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['author'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "      </div>
    </div>






";
        
        $__internal_3acb802d6453bcff7de005db56db6e02d3e892bec95b6ff6f5569230c65b784c->leave($__internal_3acb802d6453bcff7de005db56db6e02d3e892bec95b6ff6f5569230c65b784c_prof);

        
        $__internal_a9dd1d7dadbf2aaec8c9aba5cba57f243b5896e69666affe9370f886bebd014d->leave($__internal_a9dd1d7dadbf2aaec8c9aba5cba57f243b5896e69666affe9370f886bebd014d_prof);

    }

    public function getTemplateName()
    {
        return ":Author:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 41,  110 => 38,  104 => 37,  95 => 31,  91 => 30,  87 => 29,  83 => 27,  79 => 26,  65 => 15,  59 => 12,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}





    <!-- Button add task-->
    <div class=\"container\">
      <div class=\"row add\">
      <a href=\"{{ path('author_new') }}\">
          <div class=\"col-xs-12 \">
            <button type=\"button\" class=\"btn btn-default btnadd\">
              <img class=\"addimg\" src=\"{{ asset('img/add.png') }}\" alt=\"add\"/>
              <p class=\"addp\">Add a author</p>
            </button>
          </div>
        </a>
      </div>
    </div>

    <!-- Task -->
    <div class=\"container\">
      <div class=\"row message\">
        {% for author in authors %}

        <div class=\"col-xs-9\">
          <p class=\"firstName\">{{ author.FirstName }}</p>
          <p class=\"lastName\">{{ author.LastName }}</p>
          <p class=\"gender\">{{ author.gender }}</p>



        </div>
        <div class=\"col-xs-2\">
          <a href=\"{{ path('author_edit', { 'id': author.id }) }}\"><img src=\"{{ asset('img/modify.png') }}\" alt=\"edit\"/></a>
          <a href=\"{{ path('author_show', { 'id': author.id }) }}\"><img src=\"{{ asset('img/view.png') }}\" alt=\"More\"/></a>
        </div>
        {% endfor %}
      </div>
    </div>






{% endblock %}
", ":Author:index.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Author/index.html.twig");
    }
}
