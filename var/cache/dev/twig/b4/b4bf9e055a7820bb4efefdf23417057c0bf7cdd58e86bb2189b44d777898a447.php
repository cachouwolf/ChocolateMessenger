<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_fea3a793720e7e3ca582497db3bcbbd044a3fdefbfd6759e0bc06bfffe26ad1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46b120d9d54be59e06801c68e71c2eddd472853d924cdfdadc9a6da016bc60af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46b120d9d54be59e06801c68e71c2eddd472853d924cdfdadc9a6da016bc60af->enter($__internal_46b120d9d54be59e06801c68e71c2eddd472853d924cdfdadc9a6da016bc60af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_d59ee5d69eb1fdd754b7a6db9979b4fd2c87adad866e03f397029b58b791264f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d59ee5d69eb1fdd754b7a6db9979b4fd2c87adad866e03f397029b58b791264f->enter($__internal_d59ee5d69eb1fdd754b7a6db9979b4fd2c87adad866e03f397029b58b791264f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_46b120d9d54be59e06801c68e71c2eddd472853d924cdfdadc9a6da016bc60af->leave($__internal_46b120d9d54be59e06801c68e71c2eddd472853d924cdfdadc9a6da016bc60af_prof);

        
        $__internal_d59ee5d69eb1fdd754b7a6db9979b4fd2c87adad866e03f397029b58b791264f->leave($__internal_d59ee5d69eb1fdd754b7a6db9979b4fd2c87adad866e03f397029b58b791264f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
