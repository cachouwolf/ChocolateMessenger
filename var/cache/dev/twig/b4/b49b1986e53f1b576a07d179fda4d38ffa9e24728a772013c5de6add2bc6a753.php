<?php

/* :Message:edit.html.twig */
class __TwigTemplate_ef2208a915f14673fcd9abf3418a76a4c3114b34d492b22e0e478993f1c334b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Message:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56d8d8e8a8793e5454d4071774af423f8cf1f0d0e78ad12bd01d3e21ddaacdd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56d8d8e8a8793e5454d4071774af423f8cf1f0d0e78ad12bd01d3e21ddaacdd2->enter($__internal_56d8d8e8a8793e5454d4071774af423f8cf1f0d0e78ad12bd01d3e21ddaacdd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:edit.html.twig"));

        $__internal_b072707bbc0b2b5d698162ed3255928c7b39be03ea922205645dea67eb939e86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b072707bbc0b2b5d698162ed3255928c7b39be03ea922205645dea67eb939e86->enter($__internal_b072707bbc0b2b5d698162ed3255928c7b39be03ea922205645dea67eb939e86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:edit.html.twig"));

        // line 2
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme(($context["edit_form"] ?? $this->getContext($context, "edit_form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56d8d8e8a8793e5454d4071774af423f8cf1f0d0e78ad12bd01d3e21ddaacdd2->leave($__internal_56d8d8e8a8793e5454d4071774af423f8cf1f0d0e78ad12bd01d3e21ddaacdd2_prof);

        
        $__internal_b072707bbc0b2b5d698162ed3255928c7b39be03ea922205645dea67eb939e86->leave($__internal_b072707bbc0b2b5d698162ed3255928c7b39be03ea922205645dea67eb939e86_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_fb0e75ffacebb4fae1e49f97612282c3577fb3537e6492f93f18c84fe1d73f52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb0e75ffacebb4fae1e49f97612282c3577fb3537e6492f93f18c84fe1d73f52->enter($__internal_fb0e75ffacebb4fae1e49f97612282c3577fb3537e6492f93f18c84fe1d73f52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_699a3e799f108d6ee100a3944a8f6c4f881f994706def02e8132cfaf70b26716 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_699a3e799f108d6ee100a3944a8f6c4f881f994706def02e8132cfaf70b26716->enter($__internal_699a3e799f108d6ee100a3944a8f6c4f881f994706def02e8132cfaf70b26716_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <!--Form add / modify -->
    <div class=\"container\">
      <div class=\"col-xs-12\">
          <h1 class=\"title\">Modifier un message </h1>
          <div class=\"row btnedit\">
            ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
            <div class=\"col-md-10\">

              ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
            </div>
            <div class=\"col-md-2\"/>
            </div>
            <div class=\"col-md-10\">
              <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Modifier\" />
            </div>
            ";
        // line 20
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "
            <div class=\"col-md-2 icon\"/>
              <a href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_index");
        echo "\"><img class=\"icon-return\"src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/return.png"), "html", null, true);
        echo "\" alt=\"Retour\"/></a>
              ";
        // line 23
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "<input class=\"icon-delete\" type=\"image\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/remove.png"), "html", null, true);
        echo "\" alt=\"effacer\"/>";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
            </div>
          </div>


      </div>
    </div>

";
        
        $__internal_699a3e799f108d6ee100a3944a8f6c4f881f994706def02e8132cfaf70b26716->leave($__internal_699a3e799f108d6ee100a3944a8f6c4f881f994706def02e8132cfaf70b26716_prof);

        
        $__internal_fb0e75ffacebb4fae1e49f97612282c3577fb3537e6492f93f18c84fe1d73f52->leave($__internal_fb0e75ffacebb4fae1e49f97612282c3577fb3537e6492f93f18c84fe1d73f52_prof);

    }

    public function getTemplateName()
    {
        return ":Message:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 23,  80 => 22,  75 => 20,  65 => 13,  59 => 10,  52 => 5,  43 => 4,  33 => 1,  31 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% form_theme edit_form 'bootstrap_3_layout.html.twig' %}

{% block body %}
    <!--Form add / modify -->
    <div class=\"container\">
      <div class=\"col-xs-12\">
          <h1 class=\"title\">Modifier un message </h1>
          <div class=\"row btnedit\">
            {{ form_start(edit_form) }}
            <div class=\"col-md-10\">

              {{ form_widget(edit_form) }}
            </div>
            <div class=\"col-md-2\"/>
            </div>
            <div class=\"col-md-10\">
              <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Modifier\" />
            </div>
            {{ form_end(edit_form) }}
            <div class=\"col-md-2 icon\"/>
              <a href=\"{{ path('message_index') }}\"><img class=\"icon-return\"src=\"{{ asset('img/return.png') }}\" alt=\"Retour\"/></a>
              {{ form_start(delete_form) }}<input class=\"icon-delete\" type=\"image\" src=\"{{ asset('img/remove.png') }}\" alt=\"effacer\"/>{{ form_end(delete_form) }}
            </div>
          </div>


      </div>
    </div>

{% endblock %}
", ":Message:edit.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Message/edit.html.twig");
    }
}
