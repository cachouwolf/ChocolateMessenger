<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_e604c17d60b9bbb8f9cac0322a678f018970625992eb1bc06a579c5eff88914b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7e19aa1c5a6a67e9bb6c873dfe509c2c7f7b9d709315c6bd28d096e2b42efcf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7e19aa1c5a6a67e9bb6c873dfe509c2c7f7b9d709315c6bd28d096e2b42efcf->enter($__internal_f7e19aa1c5a6a67e9bb6c873dfe509c2c7f7b9d709315c6bd28d096e2b42efcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_3f70247152de24101e63e1c75e39159ffca3abeb2c263ebc702905c38128451a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f70247152de24101e63e1c75e39159ffca3abeb2c263ebc702905c38128451a->enter($__internal_3f70247152de24101e63e1c75e39159ffca3abeb2c263ebc702905c38128451a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_f7e19aa1c5a6a67e9bb6c873dfe509c2c7f7b9d709315c6bd28d096e2b42efcf->leave($__internal_f7e19aa1c5a6a67e9bb6c873dfe509c2c7f7b9d709315c6bd28d096e2b42efcf_prof);

        
        $__internal_3f70247152de24101e63e1c75e39159ffca3abeb2c263ebc702905c38128451a->leave($__internal_3f70247152de24101e63e1c75e39159ffca3abeb2c263ebc702905c38128451a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
