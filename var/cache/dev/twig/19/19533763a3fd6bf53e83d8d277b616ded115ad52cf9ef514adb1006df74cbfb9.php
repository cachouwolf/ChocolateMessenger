<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_95d7afa5dccedf33d5c5855dd7cff6cece12ad2c9179c95e5f380d3d7bc24e66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_27cadbc5e27afff7981d43e4da227e83723a92f9bdceb2da8e01f9e2e1f53cb3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_27cadbc5e27afff7981d43e4da227e83723a92f9bdceb2da8e01f9e2e1f53cb3->enter($__internal_27cadbc5e27afff7981d43e4da227e83723a92f9bdceb2da8e01f9e2e1f53cb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_ca46921764cf942b86d3f0e017ece63263dd63d279042987f52e3bdc2b7001e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca46921764cf942b86d3f0e017ece63263dd63d279042987f52e3bdc2b7001e9->enter($__internal_ca46921764cf942b86d3f0e017ece63263dd63d279042987f52e3bdc2b7001e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_27cadbc5e27afff7981d43e4da227e83723a92f9bdceb2da8e01f9e2e1f53cb3->leave($__internal_27cadbc5e27afff7981d43e4da227e83723a92f9bdceb2da8e01f9e2e1f53cb3_prof);

        
        $__internal_ca46921764cf942b86d3f0e017ece63263dd63d279042987f52e3bdc2b7001e9->leave($__internal_ca46921764cf942b86d3f0e017ece63263dd63d279042987f52e3bdc2b7001e9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
